#!/usr/bin/env node
import * as cdk from "aws-cdk-lib";
import "source-map-support/register";
import { EnvName } from "../lib/dtos/appStackProps";
import { SdaStack } from "../lib/sda-stack";

const region = process.env.AWS_REGION || process.env.AWS_DEFAULT_REGION || "eu-central-1";
const envName: EnvName = (process.env.STAGE as EnvName) || "dev";

const app = new cdk.App();
new SdaStack(app, "SdaStack", {
    env: { region },
    envName,
});
