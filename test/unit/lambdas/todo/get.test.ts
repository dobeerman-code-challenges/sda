import { Logger } from "@aws-lambda-powertools/logger";
import { Context } from "aws-lambda";
import { AppAPIGatewayEvent } from "../../../../lambdas/todo/dtos/apiGatewayEvent";
import { TodoItem } from "../../../../lambdas/todo/dtos/todoItem";
import { handler } from "../../../../lambdas/todo/get";
import { DbClientModule } from "../../../../lambdas/todo/modules/dbClient";

describe("Get item lambda", () => {
    let dbClientModuleGetSpy: jest.Mock;
    let event: AppAPIGatewayEvent;
    let item: TodoItem;

    beforeEach(() => {
        item = {
            id: "testId",
            title: "Test title",
            description: "Test description",
            __type: "todo", // eslint-disable-line
        };
        dbClientModuleGetSpy = jest.fn().mockResolvedValue(item);
        DbClientModule.prototype.get = dbClientModuleGetSpy;

        event = {
            pathParameters: {
                id: "testId",
            },
        } as AppAPIGatewayEvent;

        // Prevent logging
        Logger.prototype.debug = jest.fn();
        Logger.prototype.info = jest.fn();
        Logger.prototype.warn = jest.fn();
        Logger.prototype.error = jest.fn();
    });

    test("WHEN event has path property `id` THEN return todo", async () => {
        const result = await handler(event, {} as Context);

        expect(result).toEqual({
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Credentials": true,
                "Access-Control-Allow-Origin": "*",
            },
            body: '{"id":"testId","title":"Test title","description":"Test description","__type":"todo"}',
        });
    });

    test("WHEN item is not found THEN return empty body", async () => {
        dbClientModuleGetSpy = jest.fn().mockResolvedValue(undefined);
        DbClientModule.prototype.get = dbClientModuleGetSpy;

        const result = await handler(event, {} as Context);

        expect(result).toEqual({
            statusCode: 404,
            headers: {
                "Access-Control-Allow-Credentials": true,
                "Access-Control-Allow-Origin": "*",
            },
            body: "{}",
        });
    });

    test("WHEN id is not provided THEN return 404 error", async () => {
        dbClientModuleGetSpy = jest.fn();
        DbClientModule.prototype.get = dbClientModuleGetSpy;

        const result = await handler(
            { ...event, pathParameters: { id: undefined } } as AppAPIGatewayEvent,
            {} as Context,
        );

        expect(result).toEqual(
            expect.objectContaining({
                statusCode: 404,
                headers: {
                    "Access-Control-Allow-Credentials": true,
                    "Access-Control-Allow-Origin": "*",
                },
            }),
        );
        expect(dbClientModuleGetSpy).not.toBeCalled();
    });

    test("WHEN get method crashed THEN return 500 with error", async () => {
        dbClientModuleGetSpy = jest.fn().mockRejectedValue(new Error("test crashed"));
        DbClientModule.prototype.get = dbClientModuleGetSpy;

        const result = await handler(event, {} as Context);

        expect(result).toEqual(
            expect.objectContaining({
                statusCode: 500,
                headers: {
                    "Access-Control-Allow-Credentials": true,
                    "Access-Control-Allow-Origin": "*",
                },
            }),
        );
        expect(Logger.prototype.error).toBeCalledWith("GET", { error: new Error("test crashed") });
    });
});
