import * as cdk from "aws-cdk-lib";
import * as apigateway from "aws-cdk-lib/aws-apigateway";
import { CorsOptions } from "aws-cdk-lib/aws-apigateway";
import * as dynamodb from "aws-cdk-lib/aws-dynamodb";
import { NodejsFunction } from "aws-cdk-lib/aws-lambda-nodejs";
import { Construct } from "constructs";
import * as path from "path";
import { defaultLambdaConfig } from "../lambdas/configs/defaultLambdaConfig";
import { AppStackProps } from "./dtos/appStackProps";

export class SdaStack extends cdk.Stack {
    constructor(scope: Construct, id: string, props?: AppStackProps) {
        super(scope, id, props);

        if (!props?.envName) {
            throw new Error("environment name not specified");
        }

        /** ------------------ DynamoDB Table Definition ------------------- */

        const table = new dynamodb.Table(this, id, {
            billingMode: dynamodb.BillingMode.PAY_PER_REQUEST,
            removalPolicy: cdk.RemovalPolicy.DESTROY,
            partitionKey: { name: "id", type: dynamodb.AttributeType.STRING },
        });

        table.addGlobalSecondaryIndex({
            indexName: "byType",
            partitionKey: { name: "__type", type: dynamodb.AttributeType.STRING },
            projectionType: dynamodb.ProjectionType.ALL,
        });

        /** ------------------ Lambda Handlers Definition ------------------ */

        const lambdaDefaultEnvironmentValiables = {
            LOG_LEVEL: this.getLoglevel(props.envName),
            TABLE_NAME: table.tableName,
            SERVICE_NAME: `${id}Service`,
        };

        const createLambda = new NodejsFunction(this, "CreateLambdaFunction", {
            ...defaultLambdaConfig(props.envName),
            functionName: `${id}-create-${props.envName}`,
            entry: path.join(__dirname, "../lambdas/todo/create.ts"),
            environment: {
                ...lambdaDefaultEnvironmentValiables,
            },
        });

        const updateLambda = new NodejsFunction(this, "UpdateLambdaFunction", {
            ...defaultLambdaConfig(props.envName),
            functionName: `${id}-update-${props.envName}`,
            entry: path.join(__dirname, "../lambdas/todo/update.ts"),
            environment: {
                ...lambdaDefaultEnvironmentValiables,
            },
        });

        const deleteLambda = new NodejsFunction(this, "DeleteLambdaFunction", {
            ...defaultLambdaConfig(props.envName),
            functionName: `${id}-delete-${props.envName}`,
            entry: path.join(__dirname, "../lambdas/todo/delete.ts"),
            environment: {
                ...lambdaDefaultEnvironmentValiables,
            },
        });

        const getLambda = new NodejsFunction(this, "GetLambdaFunction", {
            ...defaultLambdaConfig(props.envName),
            functionName: `${id}-get-${props.envName}`,
            entry: path.join(__dirname, "../lambdas/todo/get.ts"),
            environment: {
                ...lambdaDefaultEnvironmentValiables,
            },
        });

        const getAllLambda = new NodejsFunction(this, "GetAllLambdaFunction", {
            ...defaultLambdaConfig(props.envName),
            functionName: `${id}-getAll-${props.envName}`,
            entry: path.join(__dirname, "../lambdas/todo/getAll.ts"),
            environment: {
                ...lambdaDefaultEnvironmentValiables,
            },
        });

        /** --------------------- Rest API Definition ---------------------- */

        const api = new apigateway.RestApi(this, `${id}Api`, {
            restApiName: `${id}-api-${props?.envName}`,
            deployOptions: {
                stageName: props.envName,
            },
            endpointConfiguration: {
                types: [apigateway.EndpointType.REGIONAL],
            },
        });

        const defaultCorsPreflightOptions: CorsOptions = {
            allowHeaders: ["Content-Type", "X-Amz-Date", "Authorization", "X-Api-Key"],
            allowMethods: ["OPTIONS", "GET", "POST", "PUT", "DELETE"],
            allowCredentials: true,
            allowOrigins: ["*"],
        };

        const v1ApiResource = api.root.addResource("v1");
        const todoApiResource = v1ApiResource.addResource("todo");

        todoApiResource.addCorsPreflight(defaultCorsPreflightOptions);
        todoApiResource.addMethod("POST", new apigateway.LambdaIntegration(createLambda));
        todoApiResource.addMethod("GET", new apigateway.LambdaIntegration(getAllLambda));

        const todoWithIdApiResource = todoApiResource.addResource("{id}");

        todoWithIdApiResource.addCorsPreflight(defaultCorsPreflightOptions);
        todoWithIdApiResource.addMethod("PUT", new apigateway.LambdaIntegration(updateLambda));
        todoWithIdApiResource.addMethod("GET", new apigateway.LambdaIntegration(getLambda));
        todoWithIdApiResource.addMethod("DELETE", new apigateway.LambdaIntegration(deleteLambda));

        /** ------------------ Granting Lambda Functions ------------------- */

        table.grant(createLambda, "dynamodb:PutItem");
        table.grant(updateLambda, "dynamodb:UpdateItem");
        table.grant(getLambda, "dynamodb:GetItem");
        table.grant(getAllLambda, "dynamodb:Query");
        table.grant(deleteLambda, "dynamodb:DeleteItem");
    }

    private getLoglevel(envName: string): string {
        return { prod: "ERROR" }[envName] || "DEBUG";
    }
}
