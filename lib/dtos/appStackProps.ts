import * as cdk from "aws-cdk-lib";

export type EnvName = "dev" | "stage" | "prod" | undefined;

export interface AppStackProps extends cdk.StackProps {
    envName?: EnvName;
}
