import { HttpStatusCode } from "../dtos/httpStatusCodes";

export const formatResponse = (
    statusCode: HttpStatusCode,
    body?: Record<string, unknown> | Error,
    headers: Record<string, unknown> = {},
) => {
    let newBody: Record<string, unknown> = {};

    if (body instanceof Error) {
        newBody = {
            errorName: body.name,
            errorMessage: body.message,
            stackTrace: body.stack,
        };
    } else {
        newBody = { ...body };
    }

    return {
        statusCode,
        body: JSON.stringify(newBody, (_, value) => (typeof value === "bigint" ? value.toString() : value)),
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true,
            ...headers,
        },
    };
};
