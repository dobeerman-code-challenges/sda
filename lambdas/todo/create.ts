import { injectLambdaContext, Logger } from "@aws-lambda-powertools/logger";
import middy from "@middy/core";
import httpJsonBodyParser from "@middy/http-json-body-parser";
import { AppAPIGatewayEvent } from "./dtos/apiGatewayEvent";
import { HttpStatusCode } from "./dtos/httpStatusCodes";
import { TodoItem } from "./dtos/todoItem";
import { DbClientModule } from "./modules/dbClient";
import { ddbDocClient } from "./modules/ddbDocClient";
import { formatResponse } from "./utils/formatResponse";

const tableName = process.env.TABLE_NAME ?? "UNDEFINED";
const serviceName = process.env.SERVICE_NAME ?? "SDA";

const logger = new Logger({ serviceName });
const dbClient = new DbClientModule(ddbDocClient, tableName, logger);

const startCreateExecution = async (event: AppAPIGatewayEvent) => {
    const { title, description } = event.body as Omit<TodoItem, "id">;

    const errorMessage = "There was an error while creating the To-Do object.";

    if (!title && !title.trim()) {
        throw new Error(errorMessage);
    }

    try {
        // eslint-disable-next-line
        const item: TodoItem = await dbClient.create({ title, description, __type: "todo" });
        return formatResponse(HttpStatusCode.Created, { ...item });
    } catch (error) {
        logger.error("CREATE", { error });
        return formatResponse(HttpStatusCode.InternalServerError, {
            error: errorMessage,
        });
    }
};

export const handler = middy(startCreateExecution)
    .use(injectLambdaContext(logger, { logEvent: true }))
    .use(httpJsonBodyParser());
