import { Logger } from "@aws-lambda-powertools/logger";
import {
    DeleteCommand,
    DeleteCommandInput,
    DynamoDBDocumentClient,
    GetCommand,
    GetCommandInput,
    GetCommandOutput,
    PutCommand,
    PutCommandInput,
    QueryCommand,
    QueryCommandInput,
    QueryCommandOutput,
    UpdateCommand,
    UpdateCommandInput,
    UpdateCommandOutput,
} from "@aws-sdk/lib-dynamodb";
import { v4 as uuid } from "uuid";
import { TodoItem } from "../dtos/todoItem";
import { UnmarshalledAny } from "../dtos/unmarshalledAny";
import { UpdateItem } from "../dtos/updateItem";

export class DbClientModule {
    constructor(
        private readonly client: DynamoDBDocumentClient,
        private readonly tableName: string,
        private readonly logger: Logger,
    ) {}

    public async get(id: string): Promise<TodoItem | UnmarshalledAny> {
        const input: GetCommandInput = {
            TableName: this.tableName,
            Key: { id },
        };

        const command: GetCommand = new GetCommand(input);
        const result: GetCommandOutput = await this.client.send(command);

        this.logger.debug("GET INPUT", { input });

        return result.Item as TodoItem;
    }

    public async getAll(startId?: string): Promise<TodoItem[]> {
        const input: QueryCommandInput = {
            TableName: this.tableName,
            IndexName: "byType",
            KeyConditionExpression: "#type=:type",
            ExpressionAttributeNames: { "#type": "__type" },
            ExpressionAttributeValues: { ":type": "todo" },
        };

        if (startId) {
            input.ExclusiveStartKey = { id: startId };
        }

        this.logger.debug("GETALL INPUT", { input });

        const command = new QueryCommand(input);
        const result: QueryCommandOutput = await this.client.send(command);

        this.logger.debug("GETALL RESULT", { result });

        return (result.Items as TodoItem[]) ?? [];
    }

    public async create(todo: Omit<TodoItem, "id">): Promise<TodoItem> {
        const id: string = uuid();
        const item: TodoItem = { id, ...todo };

        const input: PutCommandInput = {
            TableName: this.tableName,
            Item: item,
        };

        this.logger.debug("CREATE INPUT", { input });

        const command: PutCommand = new PutCommand(input);
        await this.client.send(command);

        return item;
    }

    public async update(todo: UpdateItem): Promise<TodoItem> {
        const input: UpdateCommandInput = {
            TableName: this.tableName,
            Key: { id: todo.id },
            ExpressionAttributeNames: {},
            ExpressionAttributeValues: {},
            ReturnValues: "ALL_NEW",
        };

        const updateExpression: string[] = [];

        const { id: _, ...rest } = todo;

        Object.entries(rest).forEach(([key, value]) => {
            if (!value) return;
            input.ExpressionAttributeNames![`#${key}`] = `${key}`;
            input.ExpressionAttributeValues![`:${key}`] = value;
            updateExpression.push(`#${key}=:${key}`);
        });

        input.UpdateExpression = "SET ".concat(updateExpression.join(", "));

        this.logger.debug("UPDATE INPUT", { input });

        const command: UpdateCommand = new UpdateCommand(input);

        const result: UpdateCommandOutput = await this.client.send(command);

        return result.Attributes as TodoItem;
    }

    public async delete(id: string): Promise<void> {
        const input: DeleteCommandInput = {
            TableName: this.tableName,
            Key: { id },
        };

        this.logger.debug("DELETE INPUT", { input });

        const command: DeleteCommand = new DeleteCommand(input);

        await this.client.send(command);
    }
}
