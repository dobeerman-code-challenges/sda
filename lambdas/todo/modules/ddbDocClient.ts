import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocumentClient, TranslateConfig } from "@aws-sdk/lib-dynamodb";
import { marshallOptions, unmarshallOptions } from "@aws-sdk/util-dynamodb";

const region = process.env.AWS_REGION || process.env.AWS_DEFAULT_REGION || "eu-central-1";

const ddbClient = new DynamoDBClient({ region });

const marshallOptions: marshallOptions = {
    convertEmptyValues: false, // false, by default.
    removeUndefinedValues: true, // false, by default.
    convertClassInstanceToMap: false, // false, by default.
};

const unmarshallOptions: unmarshallOptions = {
    wrapNumbers: false, // false, by default.
};

const translateConfig: TranslateConfig = { marshallOptions, unmarshallOptions };

const ddbDocClient: DynamoDBDocumentClient = DynamoDBDocumentClient.from(ddbClient, translateConfig);

export { ddbDocClient };
