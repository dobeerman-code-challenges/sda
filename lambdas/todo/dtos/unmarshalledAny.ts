export interface UnmarshalledAny {
    [key: string]: unknown;
}
