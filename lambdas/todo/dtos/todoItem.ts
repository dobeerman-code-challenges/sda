export interface TodoItem {
    id: string;
    title: string;
    description?: string;
    __type: string; // eslint-disable-line
}
