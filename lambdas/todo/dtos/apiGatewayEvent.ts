import {
    APIGatewayEvent,
    APIGatewayProxyEventPathParameters,
    APIGatewayProxyEventQueryStringParameters,
} from "aws-lambda";
import { TodoItem } from "./todoItem";
import { UpdateItem } from "./updateItem";

export type AppAPIGatewayQueryStringParameters = APIGatewayProxyEventQueryStringParameters & {
    id?: string;
    startId?: string;
};

export type AppAPIGatewayProxyEventPathParameters = APIGatewayProxyEventPathParameters & {
    id?: string;
};

export type AppAPIGatewayEvent = APIGatewayEvent & {
    queryStringParameters: AppAPIGatewayQueryStringParameters;
    pathParameters: AppAPIGatewayProxyEventPathParameters;
    body: TodoItem | UpdateItem;
};
