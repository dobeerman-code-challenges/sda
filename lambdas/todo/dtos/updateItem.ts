import { TodoItem } from "./todoItem";

export interface UpdateItem extends Omit<TodoItem, "title" | "__type"> {
    title?: string;
}
