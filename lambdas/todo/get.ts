import { injectLambdaContext, Logger } from "@aws-lambda-powertools/logger";
import middy from "@middy/core";
import { AppAPIGatewayEvent } from "./dtos/apiGatewayEvent";
import { HttpStatusCode } from "./dtos/httpStatusCodes";
import { TodoItem } from "./dtos/todoItem";
import { UnmarshalledAny } from "./dtos/unmarshalledAny";
import { DbClientModule } from "./modules/dbClient";
import { ddbDocClient } from "./modules/ddbDocClient";
import { formatResponse } from "./utils/formatResponse";

const tableName = process.env.TABLE_NAME ?? "UNDEFINED";
const serviceName = process.env.SERVICE_NAME ?? "SDA";

const logger = new Logger({ serviceName });
const dbClient = new DbClientModule(ddbDocClient, tableName, logger);

const startGetExecution = async (event: AppAPIGatewayEvent) => {
    const { id } = event.pathParameters;

    const errorMessage = "There was an error loading the To-Do object.";

    if (!id) {
        return formatResponse(HttpStatusCode.NotFound, {
            error: new Error(errorMessage),
        });
    }

    try {
        const item: TodoItem | UnmarshalledAny = await dbClient.get(id);
        return formatResponse(item ? HttpStatusCode.Ok : HttpStatusCode.NotFound, { ...item });
    } catch (error) {
        logger.error("GET", { error });
        return formatResponse(HttpStatusCode.InternalServerError, {
            error: errorMessage,
        });
    }
};

export const handler = middy(startGetExecution).use(injectLambdaContext(logger, { logEvent: true }));
