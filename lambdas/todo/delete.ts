import { injectLambdaContext, Logger } from "@aws-lambda-powertools/logger";
import middy from "@middy/core";
import { AppAPIGatewayEvent } from "./dtos/apiGatewayEvent";
import { HttpStatusCode } from "./dtos/httpStatusCodes";
import { DbClientModule } from "./modules/dbClient";
import { ddbDocClient } from "./modules/ddbDocClient";
import { formatResponse } from "./utils/formatResponse";

const tableName = process.env.TABLE_NAME ?? "UNDEFINED";
const serviceName = process.env.SERVICE_NAME ?? "SDA";

const logger = new Logger({ serviceName });
const dbClient = new DbClientModule(ddbDocClient, tableName, logger);

const startDeleteExecution = async (event: AppAPIGatewayEvent) => {
    const { id } = event.pathParameters ?? {};

    const errorMessage = "There has been an error while deleting the To-Do object.";

    if (!id) {
        return formatResponse(HttpStatusCode.NotFound, {
            error: new Error(errorMessage),
        });
    }

    try {
        await dbClient.delete(id);
        return formatResponse(HttpStatusCode.NoContent, {
            message: "To-Do object deleted successfully.",
        });
    } catch (error) {
        logger.error("UPDATE", { error });
        return formatResponse(HttpStatusCode.InternalServerError, {
            error: errorMessage,
        });
    }
};

export const handler = middy(startDeleteExecution).use(injectLambdaContext(logger, { logEvent: true }));
