import { injectLambdaContext, Logger } from "@aws-lambda-powertools/logger";
import middy from "@middy/core";
import { AppAPIGatewayEvent } from "./dtos/apiGatewayEvent";
import { HttpStatusCode } from "./dtos/httpStatusCodes";
import { TodoItem } from "./dtos/todoItem";
import { DbClientModule } from "./modules/dbClient";
import { ddbDocClient } from "./modules/ddbDocClient";
import { formatResponse } from "./utils/formatResponse";

const tableName = process.env.TABLE_NAME ?? "UNDEFINED";
const serviceName = process.env.SERVICE_NAME ?? "SDA";

const logger = new Logger({ serviceName });
const dbClient = new DbClientModule(ddbDocClient, tableName, logger);

const startGetAllExecution = async (event: AppAPIGatewayEvent) => {
    const { startId } = event.queryStringParameters ?? {};

    const errorMessage = "There was an error loading the To-Do objects.";

    try {
        const items: TodoItem[] = await dbClient.getAll(startId);
        return formatResponse(HttpStatusCode.Ok, { todos: items });
    } catch (error) {
        logger.error("GETALL", { error });
        return formatResponse(HttpStatusCode.InternalServerError, {
            error: errorMessage,
        });
    }
};

export const handler = middy(startGetAllExecution).use(injectLambdaContext(logger, { logEvent: true }));
