import { injectLambdaContext, Logger } from "@aws-lambda-powertools/logger";
import middy from "@middy/core";
import httpJsonBodyParser from "@middy/http-json-body-parser";
import { AppAPIGatewayEvent } from "./dtos/apiGatewayEvent";
import { HttpStatusCode } from "./dtos/httpStatusCodes";
import { TodoItem } from "./dtos/todoItem";
import { DbClientModule } from "./modules/dbClient";
import { ddbDocClient } from "./modules/ddbDocClient";
import { formatResponse } from "./utils/formatResponse";

const tableName = process.env.TABLE_NAME ?? "UNDEFINED";
const serviceName = process.env.SERVICE_NAME ?? "SDA";

const logger = new Logger({ serviceName });
const dbClient = new DbClientModule(ddbDocClient, tableName, logger);

const startUpdateExecution = async (event: AppAPIGatewayEvent) => {
    const { id } = event.pathParameters;
    const { title, description } = event.body;

    const errorMessage = "There was an error while updating the To-Do object.";

    if (!id || !id.trim()) {
        return {
            statusCode: 500,
            body: JSON.stringify({ error: errorMessage }),
        };
    }

    try {
        const item: TodoItem = await dbClient.update({ id, title, description });
        return formatResponse(HttpStatusCode.Ok, { ...item });
    } catch (error) {
        logger.error("UPDATE", { error });
        return formatResponse(HttpStatusCode.InternalServerError, {
            error: errorMessage,
        });
    }
};

export const handler = middy(startUpdateExecution)
    .use(injectLambdaContext(logger, { logEvent: true }))
    .use(httpJsonBodyParser());
