import { BundlingOptions, LogLevel } from "aws-cdk-lib/aws-lambda-nodejs";

export const bundlingOptions: BundlingOptions = {
    logLevel: LogLevel.INFO,
    minify: true,
    sourceMap: true,
    target: "ES2020",
    tsconfig: "../",
};
