import * as cdk from "aws-cdk-lib";
import * as lambda from "aws-cdk-lib/aws-lambda";
import { NodejsFunctionProps } from "aws-cdk-lib/aws-lambda-nodejs";
import * as logs from "aws-cdk-lib/aws-logs";
import { EnvName } from "../../lib/dtos/appStackProps";
import { bundlingOptions } from "./bundlingOptions";

export const defaultLambdaConfig = (envName: EnvName): NodejsFunctionProps => ({
    memorySize: 1024,
    timeout: cdk.Duration.seconds(3),
    runtime: lambda.Runtime.NODEJS_16_X,
    handler: "handler",
    bundling: bundlingOptions,
    logRetention: envName === "prod" ? logs.RetentionDays.ONE_WEEK : logs.RetentionDays.ONE_DAY,
});
